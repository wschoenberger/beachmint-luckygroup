
(function() {
	Mint.Utils = (function() {
		/**
		 **ready** registers callback for when DOM everything is ready.
		 * `callback [function]` The callback for the event.
		*/
		this.ready = function(callback) {
			if (document.addEventListener) {
				document.addEventListener('DOMContentLoaded', callback);
			} else {
				document.attachEvent('onreadystatechange', function() {
					if (document.readyState === 'interactive') { callback(); }
				});
			}
		};

		/**
		 **attach** binds listener to event target.
		 * `el [element]` The event target.
		 * `eventName [string]` The event to listen for.
		 * `handler [element]` The callback for the event.
		*/
		this.attach = function(el, eventName, handler) {
			if (el.addEventListener) {
				el.addEventListener(eventName, handler);
			} else {
				el.attachEvent('on' + eventName, function(){
					handler.call(el);
				});
			}
		}

		return this;
	})();
}());
