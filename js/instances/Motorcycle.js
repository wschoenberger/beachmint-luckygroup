
(function() {
	/**
	 **Motorcycle** extends `Vehicle` customizing the number of seats and wheels.
	*/
	Mint.ns('Instances').Motorcycle = (function(_super) {
		Mint.__extends(Motorcycle, _super);
		
		function Motorcycle(options) {
			options = options || {};
			this._type = options.type || 'motorcycle';
			this._name = options.name || this._type;
			this._seats = options.seats || 1;
			this._wheels = options.wheels || 2;
		}

		Motorcycle.prototype.toString = function() {
			return "[object Motorcycle]";
		};

		return Motorcycle;
	})(Mint.Abstract.Vehicle);
}());