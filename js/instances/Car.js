
(function() {
	/**
	 **Car** extends `Vehicle` customizing the number of seats and wheels.
	*/
	Mint.ns('Instances').Car = (function(_super) {
		Mint.__extends(Car, _super);

		function Car(options) {
			options = options || {};
			this._type = options.type || 'car';
			this._name = options.name || this._type;
			this._seats = options.seats || 4;
			this._wheels = options.wheels || 4;
		}

		Car.prototype.toString = function() {
			return "[object Car]";
		};

		return Car;
	})(Mint.Abstract.Vehicle);
}());