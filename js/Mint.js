var Mint = Mint || {};
Mint.ns = function(ns) {
    var ids = ns.split('.'),
        mainNs = Mint,
        id;

     // remove the first element in the name space if it's 'Mint'
    if (ids[0] === 'Mint') {
        ids = ids.slice(1);
    }

    // make objects for each element in the namespace if it doesnt already exist
    for (var i = 0, len = ids.length; i < len; i++) {
        id = ids[i];
        if (!mainNs.hasOwnProperty(id)) {
            mainNs[id] = {};
        }
        mainNs = mainNs[id];
    }
    return mainNs;
};

// bind function to a specific scope
Mint.__bind = function(fn, me) {
    return function() {
        return fn.apply(me, arguments);
    };
};

// extends a class
Mint.__extends = function(child, parent) {
	// itterate through object keys
    for (var key in parent) {
    	// filter unwanted keys
        if (parent.hasOwnProperty(key)) { child[key] = parent[key] };
    }
    // create context for contructor
    function _constructor() {
        this.constructor = child;
    }
    // inherit the parent's prototype methods
    _constructor.prototype = parent.prototype;
    child.prototype = new _constructor();
    // reference to the super class
    child.__super__ = parent.prototype;
    return child;
};
