
(function() {
	/**
	 **Vehicle** is the base for creating different types of vehicles.
	*/
	Mint.ns('Abstract').Vehicle = (function() {

		// getters and setters
		Vehicle.prototype = {
			get type () {
				return this._type;
			},
			// set type (val) {
				
			// },

			/**
			 **name** `[string]` The name of the vehicle.
			*/
			get name () {
				return this._name;
			},
			set name (val) {
				this._name = val;				
			},

			/**
			 **seats** `[number]` The number of seats.
			*/
			get seats () {
				return this._seats;
			},
			set seats (val) {
				this._seats = val;
			},


			/**
			 **wheels** `[number]` The number of wheels.
			*/
			get wheels () {
				return this._wheels;
			},
			set wheels (val) {
				this._wheels = val;
			},


		};
		
		// defaults for 'protected' properties
		Vehicle.prototype._type = 'vehicle';
		Vehicle.prototype._name = Vehicle.prototype._type;
		Vehicle.prototype._seats = 0;
		Vehicle.prototype._wheels = 0;

		function Vehicle() {
			// the parent element for the vehicle's view
			this.parent;
			// the element representing the vehicles view
			this.el;
		}

		/**
		 **appendTo** will create and eppend the representation of this class in the DOM.
		 * `targetEL [element]` The target element you wish to append the child to.
		*/
		Vehicle.prototype.appendTo = function(targetEl) {
			this.parent = targetEl;
			this.el = document.createElement('li');
			this.el.className = 'vehicle-item';
			this.el.innerHTML = 
				'<button class="btn btn-sml info-btn">info</button>' +
				'<p class="vehicle-name">' + this.name + ' (' + this.type + ')</p>' +
				'<hr>';
			this.parent.appendChild(this.el);
			return this;
		}

		/**
		 **activate** creates event bindings for the view.
		*/
		Vehicle.prototype.activate = function() {
			var _this = this;
			this.infoButton = this.el.querySelector('.info-btn');
			Mint.Utils.attach(this.infoButton, 'click', function() {
				_this.info();
			});
			return this;
		}

		/**
		 **info** alerts the name, number of seats and wheels for the vehicle.
		*/
		Vehicle.prototype.info = function() {
			alert(this.name + ' is a ' + this.type+ ' with ' + this.seats + ' ' + ((this.seats == 1) ? 'seat' : 'seats') + ' and ' + this.wheels + ' ' + ((this.wheels == 1) ? 'wheel' : 'wheels') + '.');
		};


		Vehicle.prototype.toString = function() {
			return "[object Vehicle]";
		};

		return Vehicle;
	})();
}());