

(function() {
	Mint.Utils.ready(function() {

		var Car = Mint.Instances.Car, Motorcycle = Mint.Instances.Motorcycle, vehicleAddButton, nameField, vehiclesSel, vehicleList;

		function init() {
			nameField = document.getElementById('vehicle-name');
			vehiclesSel = document.getElementById('vehicles');
			vehicleAddButton = document.getElementById('vehicle-add-btn');
			vehicleList = document.getElementById('vehicle-list');
			activate();
		}

		function activate() {
			Mint.Utils.attach(vehicleAddButton, 'click', addVehicleClick);
			Mint.Utils.attach(document, 'keydown', function(event) {
				if (event.keyCode === 13) {
					addVehicleClick();
				}
			});
		}

		function addVehicleClick(event) {
			if (nameField.value === "" || !nameField.value) return;
			var props = {name: nameField.value};
			makeVehicle(vehiclesSel.options[vehiclesSel.selectedIndex].value, props);
			nameField.value = null;
		}

		function makeVehicle(type, props) {
			var vehicle;
			switch (type) {
				case 'car':
					vehicle = new Car(props);
					break;
				case 'motorcycle':
					vehicle = new Motorcycle(props);
					break;
				default:
					console.error('Could not get selected value. event.target.value: ', event.target.value);
			}

			if (!vehicle) return;
			// add vehicle to the list
			vehicle.appendTo(vehicleList);
			// activate event bindings
			vehicle.activate();
		}

		init();
	});
	
}());
